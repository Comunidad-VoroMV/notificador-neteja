

<img src="img/logo_readme.png" style="margin-top: 2rem">

## Notificador Neteja

<p>Notificador de actualizaciones del sistema pendientes en la bandeja del sistema</p>
<p>Está en versión de pruebas</p>

## Dependencias necesarias

<p>Para su funcionamiento es necesario tener Git, y algunas librerías necesarias de Python (instaladas con pip)</p>
<p>También son necesarias algunas librerías Qt, instaladas por muchas apliaciones, entre ellas el popular reproductor de vídeo VLC. Recomendamos que instales esta aplicación para facilitar estas librerías de forma más sencilla</p>

<p>DEBIAN:</p>

```bash
sudo apt install git libnotify4 python3-pip vlc
```

<p>FEDORA:</p>

```bash
sudo dnf install git libnotify  python3-pip vlc
```

<p>ARCH:</p>

```bash
sudo pacman -S git libnotify python-pip vlc
```

<p>OPENSUSE:</p>

```bash
sudo zypper install git libnotify4 python38-pip vlc
```

<p><b>Cualquier distribución (después de instalar pip)</b></p>

```bash
pip install cryptography PyGObject PySide2
```
<p>GNOME:</p>

<p>Se necesita la instalación de una extensión que permite ver el icono en la bandeja del sistema. Podrás instalarla aquí</p>

<p>https://extensions.gnome.org/extension/615/appindicator-support/ </p>


## Instalación

<p>Ejecuta el archivo instalar.py. Se instalará los servicios necesarios y creará un acceso a la aplicación (.desktop) en tus aplicaciones.</p>
<p>Ábrela y/o añádela al listado de aplicaciones que se ejecutan en el inicio.</p>

```bash
python3 instalar.py
```
