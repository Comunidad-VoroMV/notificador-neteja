#!/usr/bin/python3

import os
import time
import threading
import gi

gi.require_version("Gtk", "3.0")
from gi.repository import Gtk, GLib, GObject

from src.aux import *


# Obtenemos la ruta de la home
home = os.environ['HOME']
path_instalacion = home + '/.ComunidadVoroMV'

branch = "dev"

# Vector de posibles gestores de paquetes
arrayGestores=["dpkg","apt","dnf","pacman","zypper"]

# Variable que guardará la posición del vector de paquetes al correcto
#    Si sn = 5, entonces no está soportado
sn = 0


# Recorrido del gestor de paquetes y averiguación de cuál es el correcto
for gestor in arrayGestores:
    if gestor=="dpkg":
        path = "/usr/bin/_neon.calamares"
        if os.path.isfile(path):
            break
    else:
        sn = sn + 1
        path = "/usr/bin/"+gestor
        if os.path.isfile(path):
            break

# Comprueba si Neteja está soportado, si es así busca actualizaciones
if not(os.path.isfile(path)):
    sn = 5
    print("NETEJA NO SOPORTADO")

def main():
  super_usuario()
  p=".password.encrypt"
  password = str(decrypt(p,key))[2 : -1]
  error = comprobar_pass(password)

  os.remove(p)

  if error == 0:

    if os.path.isdir(path_instalacion + '/notificador-neteja'):
        comando(password,"rm " + path_instalacion + '/notificador-neteja -r')

    if os.path.isfile(home + "/.local/share/applications/neteja-notificador.desktop"):
        os.remove(home + "/.local/share/applications/neteja-notificador.desktop")

    if sn != 2:
        comando(password,'systemctl stop neteja.timer')
        comando(password,'systemctl disable neteja.timer')
        comando(password,'rm /etc/systemd/system/neteja.service')
        comando(password,'rm /etc/systemd/system/neteja.timer')
        comando(password,'systemctl daemon-reload')

if __name__ == "__main__":
  main()
