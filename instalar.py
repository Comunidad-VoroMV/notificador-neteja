#!/usr/bin/python3

import os
import time
import threading
import gi

gi.require_version("Gtk", "3.0")
from gi.repository import Gtk, GLib, GObject

from src.aux import *


# Obtenemos la ruta de la home
home = os.environ['HOME']
path_instalacion = home + '/.ComunidadVoroMV'

branch = "dev"

# Vector de posibles gestores de paquetes
arrayGestores=["dpkg","apt","dnf","pacman","zypper"]

# Variable que guardará la posición del vector de paquetes al correcto
#    Si sn = 5, entonces no está soportado
sn = 0


# Recorrido del gestor de paquetes y averiguación de cuál es el correcto
for gestor in arrayGestores:
    if gestor=="dpkg":
        path = "/usr/bin/_neon.calamares"
        if os.path.isfile(path):
            break
    else:
        sn = sn + 1
        path = "/usr/bin/"+gestor
        if os.path.isfile(path):
            break

# Comprueba si Neteja está soportado, si es así busca actualizaciones
if not(os.path.isfile(path)):
    sn = 5
    print("NETEJA NO SOPORTADO")



def clonar():
    if os.path.exists(path_instalacion) == False:
      os.mkdir(path_instalacion)
    comando_sinsudo("cd " + path_instalacion + " && git clone https://gitlab.com/Comunidad-VoroMV/notificador-neteja.git -b " + branch)

def crear_desktop():
    if os.path.exists(home + "/.local/share/applications") == False:
      os.mkdir(home + "/.local/share/applications")
    with open(home + "/.local/share/applications/neteja-notificador.desktop","w+") as writer:
        writer.write("[Desktop Entry]" + "\n")
        writer.write("Categories=System" + "\n")
        writer.write("Comment[es_ES]=Este es el Notificador de la aplicación Neteja" + "\n")
        writer.write("Comment=Este es el Notificador de la aplicación Neteja" + "\n")
        writer.write("Exec=python3 " + path_instalacion + "/notificador-neteja/neteja_trayicon.py" + "\n")
        writer.write("GenericName[en_US]=Limpiador del sistema del pingüino" + "\n")
        writer.write("GenericName=Limpiador del sistema del pingüino" + "\n")
        writer.write("Icon=" + path_instalacion + "/notificador-neteja/img/logo.png" + "\n")
        writer.write("MimeType=" + "\n")
        writer.write("Name[es_ES]=Notificador Neteja" + "\n")
        writer.write("Name=Notificador Neteja" + "\n")
        writer.write("Path=" + path_instalacion + "/notificador-neteja" + "\n")
        writer.write("StartupNotify=true" + "\n")
        writer.write("Terminal=false" + "\n")
        writer.write("TerminalOptions=" + "\n")
        writer.write("Type=Application" + "\n")
        writer.write("Version=6.0" + "\n")
        writer.write("X-DBUS-ServiceName=" + "\n")
        writer.write("X-DBUS-StartupType=" + "\n")
        writer.write("X-KDE-SubstituteUID=false" + "\n")
        writer.write("X-KDE-Username=" + "\n")
    writer.close()


def main():
  super_usuario()
  p=".password.encrypt"
  password = str(decrypt(p,key))[2 : -1]
  error = comprobar_pass(password)

  os.remove(p)

  if error == 0:
    if os.path.isdir(path_instalacion + '/notificador-neteja'):
      comando(password,"rm " + path_instalacion + '/notificador-neteja -r')
    clonar()
    if sn != 2:
      crear_service(password,10)
    crear_desktop()

if __name__ == "__main__":
  main()
