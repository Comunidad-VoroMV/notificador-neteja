#!/usr/bin/python3

import os
import time
import threading
import gi
import sys

from PySide2 import QtWidgets, QtGui

gi.require_version("Gtk", "3.0")
gi.require_version('Gio', '2.0')

from gi.repository import Gtk, GLib, GObject, Gio
from src.aux import *


# Obtenemos la ruta de la home
home = os.environ['HOME']
path_instalacion = home + '/.ComunidadVoroMV'

pausa_file = path_instalacion + '/notificador-neteja/.pausa.cfg'

logo= path_instalacion + "/notificador-neteja/img/logo.png"
logo_red= path_instalacion + "/notificador-neteja/img/logo_red.png"

# Vector de posibles gestores de paquetes
arrayGestores=["dpkg","apt","dnf","pacman","zypper"]


# Variable que guardará la posición del vector de paquetes al correcto
#    Si sn = 5, entonces no está soportado
sn = 0


# Recorrido del gestor de paquetes y averiguación de cuál es el correcto
for gestor in arrayGestores:
    if gestor=="dpkg":
        path = "/usr/bin/_neon.calamares"
        if os.path.isfile(path):
            break
    else:
        sn = sn + 1
        path = "/usr/bin/"+gestor
        if os.path.isfile(path):
            break

# Comprueba si Neteja está soportado, si es así busca actualizaciones
if not(os.path.isfile(path)):
    sn = 5
    print("NETEJA NO SOPORTADO")



class SystemTrayIcon(QtWidgets.QSystemTrayIcon):
    """
    CREAMOS UN ICONO EN LA BANDEJA DEL SISTEMA Y AÑADMIMOS EL MENÚ
    """
    def __init__(self, icon, parent=None):
        QtWidgets.QSystemTrayIcon.__init__(self, icon, parent)

        self.setToolTip('Notificador Neteja')

        menu = QtWidgets.QMenu(parent)

        neteja = menu.addAction("Lanzar Neteja")
        neteja.triggered.connect(self.exe_neteja)

        config_tpo = menu.addAction("Configurar tiempo actualización")
        config_tpo.triggered.connect(self.configurar_app)

        menu.addSeparator()

        exit_ = menu.addAction("Salir")
        exit_.triggered.connect(lambda: sys.exit())
#       exit_.setIcon(QtGui.QIcon("icon.png"))

        menu.setTitle('Notificador Neteja')

        self.setContextMenu(menu)

    def cambiar_icono(self,icon):
        self.setIcon(QtGui.QIcon(icon))

    def exe_neteja(self):
        os.system("cd " + path_instalacion + "/Neteja && python3 neteja.py")

    def reabrir(self):
      Gtk.main_quit()
      main()

    def configurar_app(self):
      super_usuario()
      p=".password.encrypt"
      password = str(decrypt(p,key))[2 : -1]

      error = comprobar_pass(password)

      os.remove(p)

      if error == 0:
        def aceptar_boton(button):
          tiempo = tiempo_box.get_text()
          file = open(pausa_file,"w")
          file.write(tiempo)
          file.close()
          crear_service(password,tiempo)
          windows.destroy()

        builder = Gtk.Builder()
        builder.add_from_file("neteja.glade")
        handlers = {
            "cerrar_app": Gtk.main_quit,
            "aceptar_click": aceptar_boton,
        }
        builder.connect_signals(handlers)
        windows = builder.get_object("window_time")
        windows.connect("destroy", Gtk.main_quit)

        tiempo_box= builder.get_object("time")

        windows.show_all()

        Gtk.main()


def actualizar():
  hay_actualizaciones(sn)
  file = open('.actualizar.sn',"r")
  hay_act = file.readline().rstrip()
  if hay_act == "NO":
    return False
  else:
    return True

def main():

  def ejecucion_procesos():

    def close_progress():
        window.destroy()

    app = QtWidgets.QApplication(sys.argv)
    w = QtWidgets.QWidget()
    tray_icon = SystemTrayIcon(QtGui.QIcon(logo), w)

    tray_icon.show()

    tray_icon.showMessage('Notificador Neteja', 'Aplicación activa',QtGui.QIcon(logo),7000)

    def proceso(notificar):

      while True:
#        file = open(pausa_file)
#        pausa = int(file.readline().rstrip())*60
#        file.close()

        pausa = 15

        if actualizar():
          if notificar:
            notificar=False
            GLib.idle_add(notificacion,"Hay actualizaciones del sistema")
            GLib.idle_add(tray_icon.cambiar_icono,logo_red)
        else:
          notificar=True
          GLib.idle_add(tray_icon.cambiar_icono,logo)
        time.sleep(pausa)

    ejecutar_thread = threading.Thread(target=proceso,args=(True,))
    ejecutar_thread.daemon = True
    ejecutar_thread.start()
    sys.exit(app.exec_())

  ejecucion_procesos()
  Gtk.main()

if __name__ == "__main__":
  main()

