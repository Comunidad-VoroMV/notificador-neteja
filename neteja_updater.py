#!/usr/bin/python3

import os



# Vector de posibles gestores de paquetes
arrayGestores=["dpkg","apt","dnf","pacman","zypper"]


# Variable que guardará la posición del vector de paquetes al correcto
#    Si sn = 5, entonces no está soportado
sn = 0


# Recorrido del gestor de paquetes y averiguación de cuál es el correcto
for gestor in arrayGestores:
    if gestor=="dpkg":
        path = "/usr/bin/_neon.calamares"
        if os.path.isfile(path):
            break
    else:
        sn = sn + 1
        path = "/usr/bin/"+gestor
        if os.path.isfile(path):
            break

# Comprueba si Neteja está soportado, si es así busca actualizaciones
if not(os.path.isfile(path)):
    sn = 5
    print("NETEJA NO SOPORTADO")

if sn == 0 or sn == 1:
    os.system('apt update')

# Con dnf no es necesario, el comando sin sudo refresca base de datos y muestra paquetes a actualizar

if sn == 3:
    os.system('pacman -Syy')

if sn == 4:
    os.system('zypper refresh')
