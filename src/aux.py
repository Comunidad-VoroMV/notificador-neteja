#!/usr/bin/python3

import os
import time
import threading
import gi

gi.require_version("Gtk", "3.0")
gi.require_version('Gio', '2.0')
gi.require_version('Notify', '0.7')

from gi.repository import Gtk, GLib, GObject, Gio, Notify

from cryptography.fernet import Fernet

# Obtenemos la ruta de la home
home = os.environ['HOME']
path_instalacion = home + '/.ComunidadVoroMV'

def exe_neteja():
    os.system("cd " + path_instalacion + "/Neteja && python3 neteja.py")


def write_key():
    """
    Generates a key and save it into a file
    """
    key = Fernet.generate_key()
    with open(".key.key", "wb") as key_file:
        key_file.write(key)

def load_key():
    """
    Loads the key from the current directory named `.key.key`
    """
    return open(".key.key", "rb").read()

# generate and write a new key
# write_key()

write_key()

# load the previously generated key
key = load_key()


def encrypt(filename, key):
    """
    Given a filename (str) and key (bytes), it encrypts the file and write it
    """
    f = Fernet(key)
    with open(filename, "rb") as file:
        # read all file data
        file_data = file.read()
    # encrypt data
    encrypted_data = f.encrypt(file_data)
    # write the encrypted file
    with open(filename, "wb") as file:
        file.write(encrypted_data)

def decrypt(filename, key):
    """
    Given a filename (str) and key (bytes), it decrypts the file and write it
    """
    f = Fernet(key)
    with open(filename, "rb") as file:
        # read the encrypted data
        encrypted_data = file.read()
    # decrypt data
    decrypted_data = f.decrypt(encrypted_data)
    return decrypted_data
    # write the original file
    #with open(filename, "wb") as file:
    #    file.write(decrypted_data)

# Ejecuta un comando del sistema como si estubiera en el terminal con sudo. No muestra mensaje en pantalla
#   y manda la salida al fichero de la variable definida fich_sal
#  Parámetros:
#     cmd: comando a ejecutar
def comando(password,cmd):
    error = os.system('echo %s|sudo -S %s' % (password, cmd) + ' 2>&1')
    return error

# Ejecuta un comando del sistema como si estubiera en el terminal sin sudo. No muestra mensaje en pantalla
#   y manda la salida al fichero de la variable definida fich_sal
#  Parámetros:
#     cmd: comando a ejecutar
def comando_sinsudo(cmd):
    error = os.system(cmd)
    return error

# Comprueba si la contraseña de superusuario es correcta creando un fichero vacío en HOME llamado test_neteja.kk
#   y manda la salida en la variable definida fich_sal
#  Devuelve el error del comando del sistema (si es 0 entnoces no ha habido problema,)
#  Parámetros:
#     password: contraseña de superusuario
def comprobar_pass(password):
    cmd = 'dd if=/dev/null of=$HOME/test_neteja.kk bs=500M count=1  >> $HOME/neteja.log 2>&1'
    error = os.system('echo %s|sudo -S %s' % (password, cmd))
    return error

# Pantalla de petición de contraseña
#  Permite hasta 3 errores, si se falla más sale de la app cargando la pantalla de error
#  Si se introduce bien, carga el módulo de pantalla principal
#  Parámetros:
#       gestor: gestor de paquetes en texto
#       sn: código del gestor en el array
def super_usuario():
    password=""
    error = 0
    def cargar_ventana(errores):
        def aceptar_boton(button):
            password = password_box.get_text()
            error = comprobar_pass(password)
            if error == 0:
                windows.destroy()
                p = ".password.encrypt"
                with open(p, "w+") as file:
                    file.write(password)
                encrypt(p,key)
                # Parece que todo va bien, se devuelve password
            else:
                errores = n_error + 1
                if errores == 3:
                    windows.destroy()
                    # Se ha introducido demasiadas veces la contraseña mal y se carga la pantalla de errores
                    ventana_error_pass()

                else:
                    windows.destroy()
                    cargar_ventana(errores)

        n_error = errores

        if n_error == 0:
            msg_error = ""
        else:
            msg_error = "Contraseña incorrecta (intento " + str(n_error+1) + "). Inténtelo de nuevo"

        builder = Gtk.Builder()
        builder.add_from_file("neteja.glade")
        handlers = {
            "cerrar_app": Gtk.main_quit,
            "aceptar_click": aceptar_boton,
        }

        builder.connect_signals(handlers)
        windows = builder.get_object("super_usuario")
        windows.connect("destroy", Gtk.main_quit)
        etiqueta_error = builder.get_object("etiqueta_error")
        etiqueta_error.set_text(msg_error)
        password_box =  builder.get_object("password")
        password_box.set_text("")

        windows.show_all()

        Gtk.main()

    cargar_ventana(0)

# Pantalla de error en la contraseña. Se saldrá de la aplicación
def ventana_error_pass():
    builder = Gtk.Builder()
    builder.add_from_file("neteja.glade")
    handlers = {
        "cerrar_app": Gtk.main_quit,
    }
    builder.connect_signals(handlers)
    windows = builder.get_object("error_pass")

    windows.show_all()

    Gtk.main()

def crear_service(password,pausa):
    with open("neteja.service","w") as writer:
        writer.write("[Unit]" + "\n")
        writer.write("Description=Notificador del Limpiador del sistema Neteja" + "\n")
        writer.write("" + "\n")
        writer.write("[Service]" + "\n")
        writer.write("WorkingDirectory="+ path_instalacion + "/notificador-neteja" + "\n")
        writer.write("ExecStart=/usr/bin/python3 " + path_instalacion +"/notificador-neteja/neteja_updater.py" + "\n")
        writer.write("" + "\n")
        writer.write("[Install]" + "\n")
        writer.write("WantedBy=default.target")
    writer.close()

    with open("neteja.timer","w") as writer:
        writer.write("[Unit]" + "\n")
        writer.write("Description=Timer de Neteja" + "\n")
        writer.write("" + "\n")
        writer.write("[Timer]" + "\n")
        writer.write("OnBootSec=1min" + "\n")
        writer.write("OnUnitActiveSec=" + str(pausa) + "min" + "\n")
        writer.write("" + "\n")
        writer.write("[Install]" + "\n")
        writer.write("WantedBy=timers.target")
    writer.close()

    comando(password,'mv neteja.service /etc/systemd/system/neteja.service')
    comando(password,'mv neteja.timer /etc/systemd/system/neteja.timer')
    comando(password,'systemctl stop neteja.timer')
    comando(password,'systemctl daemon-reload')
    comando(password,'systemctl start neteja.timer')
    comando(password,'systemctl enable neteja.timer')

def notificacion(txt):

    class App():
        def __init__(self,loop):
            self.loop = loop
            self.n = None
            Notify.init("Notificador de Neteja")
            self.check()
        def check(self):
            with open(".actualizaciones.txt","r") as f:
                txt2 = f.read()
            f.close()
            logo=path_instalacion + "/notificador-neteja/img/logo.png"
            self.n = Notify.Notification.new(txt,txt2,logo)
            self.n.add_action("action_click", "Lanzar Neteja", self.neteja, None)
            self.n.show()
            GLib.timeout_add_seconds(60, self.handler)
        def handler(self):
            self.loop.quit()
        def neteja(self,notification, action_name, data):
            os.system("cd " + path_instalacion + "/Neteja && python3 neteja.py")
    loop = GLib.MainLoop()
    app = App(loop)
    loop.run()

def hay_actualizaciones(sn):
# Vector de posibles gestores de paquetes
#arrayGestores=["dpkg","apt","dnf","pacman","zypper"]

    # ACTUALIZACIONES CON APT
    if sn == 0 or sn == 1:
        file = path_instalacion + '/notificador-neteja/.actualizaciones.txt'
        os.remove(file)
        os.system('apt list --upgradable >> ' + file)

        with open(file) as f:
            next(f)
            hay_act = f.read()
        f.close()

        file = open(path_instalacion + '/notificador-neteja/.actualizar.sn','w+')
        if hay_act == "":
            file.write("NO")
        else:
            file.write("SI")
        file.close()

    # ACTUALIZACIONES CON DNF
    if sn == 2:
        file1 = path_instalacion + '/notificador-neteja/.actualizaciones_dnf.txt'
        file2 = path_instalacion + '/notificador-neteja/.actualizaciones.txt'
        if os.path.isfile(file1):
            os.remove(file1)
        if os.path.isfile(file2):
            os.remove(file2)
        os.system('dnf check-update >> ' + file1)
        string = 'update'

        f = open(file1, "r")
        flag=0
        index=0
        for line in f:
            index += 1
            if string in line:
                flag = 1
                f.seek(0,0)
                for i in range(index-2):
                    next(f)
                resto = f.read()
                break
        f.close()
        file = open(path_instalacion + '/notificador-neteja/.actualizar.sn','w+')
        if flag == 0:
            file.write("NO")
        else:
            file.write("SI")
        file.close()

        file = open(file2,'w+')
        file.write(resto)
        file.close()

    # ACTUALIZACIONES CON PACMAN
    if sn == 3:
        file = path_instalacion + '/notificador-neteja/.actualizaciones.txt'
        os.remove(file)
        os.system('pacman -Qu >> ' + file)

        f = open(file,"r")
        hay_act = f.read()
        f.close()

        file = open(path_instalacion + '/notificador-neteja/.actualizar.sn','w+')
        if hay_act == "":
            file.write("NO")
        else:
            file.write("SI")
        file.close()

    # ACTUALIZACIONES CON ZYPPER
    if sn == 4:
        file = path_instalacion + '/notificador-neteja/.actualizaciones.txt'
        os.remove(file)
        os.system('zypper lu >> ' + file)
        num_lineas = len(open(file).readlines(  ))
        if num_lineas >3:
            f = open(file,"r")
            for i in range(4):
                next(f)
            hay_act = f.read()
            f.close()
        else:
            hay_act = ""

        file = open(path_instalacion + '/notificador-neteja/.actualizar.sn','w+')
        if hay_act == "":
            file.write("NO")
        else:
            file.write("SI")
        file.close()


